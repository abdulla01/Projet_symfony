<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'App\Controller\SecurityController' shared autowired service.

include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/Controller/ControllerTrait.php';
include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
include_once $this->targetDirs[3].'/src/Controller/SecurityController.php';

$this->services['App\\Controller\\SecurityController'] = $instance = new \App\Controller\SecurityController();

$instance->setContainer(${($_ = isset($this->services['service_locator.ouuj7h0']) ? $this->services['service_locator.ouuj7h0'] : $this->load('getServiceLocator_Ouuj7h0Service.php')) && false ?: '_'}->withContext('App\\Controller\\SecurityController', $this));

return $instance;
