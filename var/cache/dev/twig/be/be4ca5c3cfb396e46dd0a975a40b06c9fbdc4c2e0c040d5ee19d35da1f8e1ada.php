<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* property/show.html.twig */
class __TwigTemplate_2de5d74fba3ff84570bc3eb66f337413b1b07236c53ed186e9756d4fc147f095 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "property/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "property/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "property/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 3, $this->source); })()), "title", [], "any", false, false, false, 3), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "  <div class=\"jumbotron\">
    <div class=\"container\">

    ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 9, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 9));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 10
            echo "       <div class=\"alert alert-success\">
            ";
            // line 11
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
       </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "    

    ";
        // line 15
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), "vars", [], "any", false, false, false, 15), "valid", [], "any", false, false, false, 15)) {
            echo " 
       <div class=\"alert alert-danger\">
         Erreur un champ est mal saisit
       </div>
       ";
        }
        // line 19
        echo "     



      <div class=\"row\">
        <div class=\"col-md-8\" style=\"background-image: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSExIVFRUVFRUVFRUVFRUVFRUVFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQGi0fHR0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tNy0rN//AABEIAKgBLAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAAIEBQYBBwj/xABJEAABAwEEBgYFBwoFBQEAAAABAAIDEQQSITEFBkFRYZETInGBocEyQnKx0QcjM1KywvAUJGJzgpKis+HxU2ODw9IVFkNUkxf/xAAZAQEBAQEBAQAAAAAAAAAAAAACAQMABAX/xAAiEQACAgICAgMBAQAAAAAAAAAAAQIRAxIhMQRBE1FhMiL/2gAMAwEAAhEDEQA/ANAJE8SqAJE4Sr4x9WicZkCS04lCMiYXrjqJLbVxRBa1ANNybQbCVbJSLmC01SM+KrIXEVx8lx78Tn3CvvV2JqWwlRBJiqQWsDYf2ifKikR281wNMdmCVkcS5AO7nh713v8AP+irm2pPFpStBpk68OJ8F0TbgOVfeoPTrvSriEwzk5kpvSKN0i7fXHEjpEjLh3qPeTHuVOJfS1UpsjSwDaL3iMKKqEmSJ+UbKd/cnF0GSJCVEFkyI2RWiNhAF26utcE4JUFsZcSuIlxTGQDcqohciqfHiu9Gps0XWP42LnRLtSbkIxpj41P6JNMKup2xWGJNMStDCmmzqanblQ6JMbHird1mTG2XFXU7czuljWmwgUPPCh3KmlK1Wm7I5xDWsJo3YCSa47Fm7VYng0Iu+0Qz7RCLjyaRmqORnBdLkaOyOoMCfZa6TxjBXTYn7Gv/AHaeDiD4K6s7cJ0bkuspVEgF46PVZEvnaFzp+1THNQjGuo6wH5Qu/lCc6EbkM2cK6nWIzbgF1svchug4ppgO9dqdZI6VcvqMWOTSXbldTieyfFFE6qulxThOqkQtBaE8WlVXTLvTKkZbi0p4tKpunS/KFUFovW2gJs1sjb6TiODWPeduxoKpRaihWic51pTfhWuyvNNAaL5tvjwusmk4BrWHvEhbREZIXOd1Lo9XrAuOGNQMM67Vm4dJUI6zN9L7anPECqmw21+xrjxDJHDH2WkJV+AdmgjZgnhihWWeQ0pC8/us8HuarWz2eV3qsHtyAeLA9JRA5AQSiNmKsIdESOGMkLD+3KP9tS49XSR1px2xxhv8xz1ooA3Klk6mx2qv9FYM1bjpRz5X8bzYzzhaxEZq9AM2F/62SWX+Y4q6h2KxwOZBA3kEDmmNnjOAkYTuD215VqryHRkDDVtniad7Y2A8wFNEg7FUkGzONaT6sn/zkpzDaIn5JJ/gyHj82PBzwfBaEPG9OT1R1mfboyX6jKcZCDyDCPFHboh/1oxwLHu8Q9vuVykrqjrKsaGG2Rw9lsdOT2uPiit0RHtvk7w9zPBhAU9JdRCtm0PER9Gx3F7b55uqVElsF3BoDR+iAB4K9Q5HDbRFo6zI2qyneVWSWfFa21MbsVLa433uo2Mimbnuaa9gYcMkaKpGYup1E6iVF80+mBmHVPYVV2u3RxFofKGXhUXgaGlK4jtCuJG4FYnXs0EB2EPH8uiUY26I3SL1mkozlPEf2h8SpDZScrp7DX4LzqyTwZyVwrlSuJr8VZRWiwbQ/t/stvi/TP5K9Gxlbe9KOvL4pkV0YhpFfaI3qq0PaLMZWiFz7xvVDi6lLp2ZblPtMsbS2/MIyWtoC4gEY40r+KI6O6L8i1uiUZW71y+N6iMtDD6NpYe9pSlrce4SRuutLsQDWnYr8cg/NEkkguphShPiuOgG5Oa3r8KH7qOWqI0ZAdZgmGznep5amlqQbK6SF31nD2btf4gUwWces+U/tXfsAKyLEJzFUSyIyzw7Q4+3JI/we4o9nhs7AS2ONrqtwbG3rYOqSQMxh+8Vx8ajTNFcfckFstzaS0NOIDgSDkCA4tJHKncuG3cdg9ypppata29UNqGitAATePiSUS2R3XANkDgWRmoIIBdG0ub2gkg8QuoOxbjSB3ordKuG1ZwOcDTvxRI3u2pJEbNNHp142qXDrO8bVlWg7/BEEfFLkDSNmzXBwBxxoadtFnTrdbHPqbQbv1Q1rdm8NBz4qudFgcTkVXtCMpNHRgmb21aetLWNMVoa12BIlBe1wu5ZEjHaEKLXyduEsUT+MbiPA/BUEUvSMFdhp/C34pfkwOxJWZ0jWw69QuzY9vZQhSWa1xnK9yWXs1lA2Ke260VcQBvKasLo0DNZd17kVJj0845V5FZW1WloAoQakNwNc1Z2T0QqmQuxpaTcfBd/6jIf7hVrXo8ILslSELTGtjbO65JWtAcKZFU0vygR7nc03WXU2W1TGQWhjG3WgC65xwGO0Kp//MXbbYO6An/cCjv6LSJU3ygt2M/i/ooUmvpr6A5oo+TJvrWt57IgPe8pH5M4f8eb91vwXcnUi1ouJ1El80+mNIWI+UBnzVnNaUe4V3VaP+K3JWM+UFn5vEf86nNknwTx/wBIj6MG5x3ndnuTCTvPMogCZKMF6zMuNTH0tkWWIeMh/huPkrHX6ofD+rI5OVPqg789g9p2/bG8K6+UYdaA8Jhycz4oP+0cujJmQ/gke4qe2pie6ucDqgEnFpOJrtpRVlDxVxBUwEdb6GcY3buFT1aY9tVoBo9KaOsOw/dRy1R4DW4d7feApdF5UasEWppajEJpCSCALUxzVIITXNSI2RXNWb1sqLlCRiciR7lqnNWY1wGDO0+4pLsEuin0VO8ysaXuIJNQSaHquI8aLjrZIHvF7AF9Oq3ZWmxc0P8ATs7T9ly49vXl/wBXzWi7M30aHRZLo2OObmNJ2YkAqdGxRNDN+Zj/AFbPshWLAoU6xqIAutCa4u2DmfgqSxS+iew+5U4crMseQQ4gA4dUUOPaSo40Wz6zuY+CElY4SSLGzAUoN/kFMY1V8LS3I17VKjlP4/umjFljEE3SPod4QWTplumq0e0PcUrDR2DMdo960EB6oWdhdiO1XMU2ARgdInByk2R+arBMpdjkz7vNaBPOdfNO2mOWXo7RM0CS6A2R7QBlgGkLz+1awWyp/PLSe20TH7y1evrqyycbQ8cr/wAFhqVkA3vA8QFIjPS4dEAjrT2p3tWmXyIRDoGDaJTxM9o/5qXZzgPxtKNVIysvqLlESi5RfO1Pp2Moslr3F+aezMDzvj7y2F1ZjXlv5nIdz2fzAPNWK/0jm+DzQJsowyTwmytwP9F6gErVfC2QfrAMt4I81pPlGZ1YDudMN+ZafJZbQZpabOf8+LZvkaFr/lDiHRxupj0pFeF1xp7uSMv7Ry6ZhKfihVxoxouU6tSyYYNIPo7TtVT+M1f6FbWjRUtrIBi0txZkNtfBMMje6PNWQu3xs8WBTyFWaENbPZjvii8YlcUWFDsEQm0Ri1cLFaDYEtTS1HuIVob1Xdh9ytBsC5qy+uYwZ2+RUJ1rcQOseZ4fFTdC6Kkt7THEQXMlIN4kDIDjvSoLkUeh2/PM7T9kp9w9JLhslPcQSEcSWaC9IbXE50T7hja2bpCTVpuh0YqBQ1NaYcRWxsJsT3tLLax8kzJg2Po5G3SYnkBz3CgN4XQDSuzCldIp2ZuSolaH+ijH+Wz7IU9qj2CG61o3NYOTQpTURBWBIhHhYnmJKgWQnIZKnuhQXwqUWyMHIgckGIwiUo6xl4oc8mXtBTGwqFpKOlz2wuIHhmBxBriR3gkEcwrFsiorAfSH+Y7+KjvvK2YVxWTmSKwsD8+7zVSxysrAK/vD3hOIGeW65urIeNokd9v4rFwYzs/Ws+2FrdZ21lDt7pMKca1qsnYKdPHXISNPJwPkrErPVYDgOzzKNeUeyODgCDhRSUjM01FyickvLqe+xtFnNcmVsc43Fp5PjK0qq9J2RsrJYnEgPAFQKkYDEclK6Os8daUn5H+i9BZqPZxnJKeyg94UlmqNlHqPd7Tx5UWtoFs810X9PCd0sRy3PaVu/lEiJhbTZOP5TlcWbV2ysNWwNqMiS4nxVm6InNoONcRXGlEW7aOTPJ4IyQPmWnuds71favwkSNrAGkuzuFuBoKlxFKLdCDgB2ABOER3/AI7gnt+Aa/Sv0TEegiaRQiOOoOYIbiDyVpRcZHT8du3vT6IqIrGEJUTqLiVEsQao9oPVIG6mPZxVLrjK5sTS1zmm+BVri05O2grNQaXtLcrRN3yPd4ElJQDsFi0FPUdXAUJzpn+jWuStNDaNtVmZL0UxidI5zr7Qai8BhiRtCzmldMWgkVnkxbU0eW7SPVpuVRaZnOpec52HrOJ2neu1ZLRMtmpobUutsQJzv0aSdpPXJqgDQkMYDjaIyQ7Bw6Qi82hNA1vFu3aohHV/aPuamyPN0DYHOPeQ0H7I5Jcv2FUjVnWmCNoqXvOFSAACab3HgujWGV/0VklfXI9c/ZaRs3rPau6Qkhna6J10u6juqx1Wk1pR4NMWjEYrex6xz7ejd2xR/dAXLGiuTI9httucBWyBvF0ob4YnwUp9otn/AKzT7NpA+1GjN1mlGcUBoCfQeMhXY8KZDrMRUGBmB9V0rfvFLRgsqJdKzxgl9lc0DEnpoHYcwUODTz5aNZBPedUNutiJrwDnDxV+7T8ZNXWUH/VPmwp8embMOv8AkxaQcwYyRgf0BsBU0Z1oyw0Zauk6Vxt4yq3o7PdoNlxryB3CqsI9Os+pMKYGtnkwO40yK1X/AHFAc2TdzYz98J7dLWY7XjtjH3XFdo/o7YyzdYoQes4tFMzHMDXbhcIQbTpaCctETw4tNXelWmyt5o4rV23TFljjdIZT1Rl0cgqagAVoQMSFkNIa6uOEUYHF2f8AXwRcaOQawYuf7Y/lsV0xizuibc+XrPNXUrhlS88U/hWlgJKHsTHtarjRLMP2h5KBGxWdjF1h7zyA+C0SM2zxrTzquj4iQ/Y+KzOhWXrTGDkXGvZQrR6azZwY/wAbvwVBqwK2yIcX+EbiuiN9Hp9nbgnujBzCJDEidCnRjZokl1cWFHtsSEYxWtEVNVoljbg3JXU5cVolnE0hPTSrRLOLicuLqJYlxdXFaJZwhNKchNcaYihVollBrk35ge233OWPYFsta4yYMqkPBw3Y/FY2MrRLglkLSnpN9n7zlBk2dnmVO0qes32fvOVVPamimOzZjniPeg+xBHej3n7qE92HPyQxOXYBhpXM8abO5No4qWjqbJeivpme15LYNWN0ZF89GdzvIrZMThyGXAQ5HsPuKMD1ne0UB+R7D7kRhxd7R8k6BYeq7I7qn8eq9MXJT1T2/dcrRCaHIrSoociNerQbAawO+Yd3fbYsoXLQ6xy/MHtH2mrJGZY5OzSHRrtXjl7A/mS/ELYWV2S8us2sDIg30i4AgtDTtdUY5bfFXuitcgWlz47oaQD1s+IBFdiwp2aPo9HiCkSvpFIdzH+DSVmbBrLG4B1H0ORoPipthtvSaPlm+s21kdl+YN8AFomjNpo8w04/EDdHXmT8FW6iRX7czg2Q/wAJHmjafm64un1KEbsSfNLUY9Dag+leo4UOGZbtXRdCknR63FZ0nxgIEGm2U6zXDsIPvomT6UYT1TXuIxWlmGrLtIpLiFHqs6mlIuTWvrkuoljklxJWiWIphcK02n+vwKcSo89pjZi9zW7i4gcqrqODpKon1hhbkS72Qfe6gVfPrI8+iwDi4k+Ap71LSKoyZpnFBltDWirnADeSAOZWPl0nM71yODQG+Ix8VEc2pqTU7zieaLmhrEzVz6ciGTq+yCfHLxVbPrEfVZ3k08B8VSliaEd2NYkO0lpWVzTV1ODRTxz8VlrTY2vNTUnDE9gHkr22CrcFCugIObNY4o/RVvsNXB1TgG0rjS6AMK9iTbEBu2DjgKeSsXtTbn4CDmxrFFdIhGyrj7Mp4GHkkG8FNhaIgRWZwIIIBGRUx9vnaG3S12GN4DedgpsojMZvR22euTU45WjOeGLIrdMy0N6IEEUqCR8VJGnmNoXMf1utgKgYltDWn1fFP/6eONUySw1oN29aLyX9GL8ZemSY9PwH1y32muHjSikm3Rub1ZGnEesNzlUSWCmyqjusXVLbpxINBXGl74rReQvZnLxmumaxrwcjVEBWLmsBvFwvNJpkdwA8koTMMGzP7yT71ovIgZvxpmg1md8x+0PeFjw4VArSqtbZNK9jWufe6xLiQ3ZdoBQdqiWezBmQqd5WWXJFu0aYsUkuSwsNlFctmZzVpZrCzaKqLYirSAry3ybtEpkQpThTBFs9seywmEXQzo3tyNQHE1oa8UONyVtwhcBup4rVMycTEWpgBNEbQNGyV4fBK1MRNGtoaoqRrKPBp2y4KPLM4HDJBY5OOK02MND0uqSprRrFEPRvO9ltBzdRV0+sch9FjW8XEuPktnKKCoSZpnOG9AntUcY6zmt7SBXuzKx0+kpn+lI7sb1R/DSqiV4IPIjRYX7NXaNYoh6N53stoObqeCrp9YpD6LWt9ol3gKBUtUrqDmxrFElz6QlfnK7sb1B/DSqh0HPmnXUrqDkaKH0cqurtFwqbF1O1XO9IlKiOwtTh/FVw1TgnXOKmxVEhWlpIUQQK1ewIfRIOQ4xILoQE7o6jLn8ApbowEN53I2x0RnwkCpy34ALoiFKk4cMFLslpew1a4tOXVJGC44E4k1O/b3lX0HmxkLRsHeURxOxcDwBv/G9Dzz5KIrCM9rxTq7uaG1vcng7glYaHUHauZ5DvXHOAzxO5OFSMcFbJQMtCGYwcKI12pTsBgM0uA0yHLZ2geSj9EMzgFNtLqCqjsjLsTlsCEmJIPZ+AUtpohwNpijNbXsVTM5IJG9EtvoJoomWx9WpXwDXkzdoGKJYW41TbQ3GiJANgUTG0Tw5SI8lCClx5J2Z6h0iu0XQ3gq2aUNouABEDErvBByEojF0cSnlu8rl7cFNhaiHYmudRccd55LgB2DzKlloRemFyvNGao2qehbC4A+s/qN8cT3LW6N+TUChnlr+jGPvu+CjmjOUox7Z5ywFXOjtWLVPQshIafWcLrebs+6q9Z0Zq5ZoMY4Wg/WPWd+87EdytQjbZk/IXpHk0/wAn1raKjo3cA417qgKitei5YjSSJ7eLgac8l7rVcfGHCjgCDsOSnL6Oj5LXaPn94G/uCG5rvxmvYdOavaOa0vlDIf0g64a9nrcl5npWzxdJcsrpJRsqzE+yBieQXSTj2enHkU+ildDTtTLis7VoqdmL4nju99Mu9QXM34KWaArgGfwTXGuAGCe8jcO9cDDuXWdQ1rU7oynNoM0XP4BdsdQAN3rpNcBgjFM7FdiUcuBvEpBm0ojW7UwtJKuxGhpNchgkRTtRHupsQaVSQGDkZtKC1+OA5o8kVURsIaFzqzkJhrmpbSNijRNqpQauCMJKFbMApCBaArYa5KXo6lGjZROnw8k2A1RsYUIzXoICdVNGbRZjgE6m8pJLrNULsHNdLT/ZJJBuhJCbHuFTwVvo/VC1z4iJzWn1pOoORxPcEkkbZlmyOHRq9GfJowUM8xd+jGLo/eOJ5Ba3RugbNZ/ooWg/WIvO/eOKSSVHjlklLtliUr24d5SSSXDAd7Vx1OwcUklNruzjP6V1xslnqL/SO+rH1uZyHNZi0a2261m7ZouiZ9YCp73u6o7gkks5TajwfQWCEOewVm1RLz0lqmc92ZAcTze7yorGG32aznorNEZZD6sLbx/af/dJJYw/06Y5P/Lb9EyPQtstP07xZoz/AOOKjpSNzn5N7q9in/8AY9iuXOhrveXOLyd96uHYMEkl7FiinR4JZ5v3RQW/5L2YmGZwOxsgDh2Aih51WV0lqfbYa1ivt3x9Ycs/BJJHNjUY7I0x+TO6fJSOhumjmkHcRQjuKafwAuJLzp2fROXTt5J3cuJKnCr+Pgk51AkkkEjOkJKIxlUklr0jL2HDAENzKpJIWWgrBRdLkkk4ozkdjbVAtfBJJc+yrorZ2k5p8bKJJLmUJdwQykkqgM//2Q==');\">
        </div>
        <div class=\"col-md-4\">
          <h1>";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 27, $this->source); })()), "title", [], "any", false, false, false, 27), "html", null, true);
        echo "</h1>
          <h2>";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 28, $this->source); })()), "rooms", [], "any", false, false, false, 28), "html", null, true);
        echo " pièces - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 28, $this->source); })()), "surface", [], "any", false, false, false, 28), "html", null, true);
        echo " m²</h2>
          <div class=\"text-primary\" style=\"font-size: 4rem;font-weight: bold;\">";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 29, $this->source); })()), "formattedPrice", [], "any", false, false, false, 29), "html", null, true);
        echo " €</div>
          <a href=\"#\" class=\"btn btn-primary\">Contacter l'agence</a>
           <div id=\"contactForm\" class=\"mt-4\">
            ";
        // line 32
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 32, $this->source); })()), 'form_start');
        echo "
            <div class=\"row\">
              <div class=\"col\">";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 34, $this->source); })()), "firstname", [], "any", false, false, false, 34), 'row');
        echo "</div>
              <div class=\"col\">";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 35, $this->source); })()), "lastname", [], "any", false, false, false, 35), 'row');
        echo "</div>
            </div>
            <div class=\"row\">
              <div class=\"col\">";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 38, $this->source); })()), "phone", [], "any", false, false, false, 38), 'row');
        echo "</div>
              <div class=\"col\">";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 39, $this->source); })()), "email", [], "any", false, false, false, 39), 'row');
        echo "</div>
            </div>
            ";
        // line 41
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 41, $this->source); })()), 'rest');
        echo "
            <div class=\"form-group\">
              <button class=\"btn btn-primary\">Envoyer</button>
            </div>
            ";
        // line 45
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 45, $this->source); })()), 'form_end');
        echo "
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class=\"container mt-4\">

    <p>
      ";
        // line 54
        echo nl2br(twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 54, $this->source); })()), "description", [], "any", false, false, false, 54), "html", null, true));
        echo "
    </p>

    <div class=\"row\">
      <div class=\"col-md-8\">
        <h2>Caractéristiques</h2>
        <table class=\"table table-striped\">
          <tr>
            <td>Surface habitable</td>
            <td>";
        // line 63
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 63, $this->source); })()), "surface", [], "any", false, false, false, 63), "html", null, true);
        echo " m²</td>
          </tr>
          <tr>
            <td>Pièces</td>
            <td>";
        // line 67
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 67, $this->source); })()), "rooms", [], "any", false, false, false, 67), "html", null, true);
        echo "</td>
          </tr>
          <tr>
            <td>Chambres</td>
            <td>";
        // line 71
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 71, $this->source); })()), "bedrooms", [], "any", false, false, false, 71), "html", null, true);
        echo "</td>
          </tr>
          <tr>
            <td>Etage</td>
            <td>";
        // line 75
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 75, $this->source); })()), "floor", [], "any", false, false, false, 75), "html", null, true);
        echo "</td>
          </tr>
          <tr>
            <td>Chauffage</td>
            <td>";
        // line 79
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["property"]) || array_key_exists("property", $context) ? $context["property"] : (function () { throw new RuntimeError('Variable "property" does not exist.', 79, $this->source); })()), "heatType", [], "any", false, false, false, 79), "html", null, true);
        echo "</td>
          </tr>
        </table>
      </div>
      <div class=\"col-md-4\">
        <h2>Spécificités</h2>
      </div>
    </div>
  </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "property/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 79,  223 => 75,  216 => 71,  209 => 67,  202 => 63,  190 => 54,  178 => 45,  171 => 41,  166 => 39,  162 => 38,  156 => 35,  152 => 34,  147 => 32,  141 => 29,  135 => 28,  131 => 27,  121 => 19,  113 => 15,  109 => 13,  100 => 11,  97 => 10,  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title property.title %}

{% block body %}
  <div class=\"jumbotron\">
    <div class=\"container\">

    {% for message in app.flashes('success') %}
       <div class=\"alert alert-success\">
            {{ message }}
       </div>
    {% endfor %}    

    {% if not form.vars.valid %} 
       <div class=\"alert alert-danger\">
         Erreur un champ est mal saisit
       </div>
       {% endif %}     



      <div class=\"row\">
        <div class=\"col-md-8\" style=\"background-image: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSExIVFRUVFRUVFRUVFRUVFRUVFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQGi0fHR0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tNy0rN//AABEIAKgBLAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAAIEBQYBBwj/xABJEAABAwEEBgYFBwoFBQEAAAABAAIDEQQSITEFBkFRYZETInGBocEyQnKx0QcjM1KywvAUJGJzgpKis+HxU2ODw9IVFkNUkxf/xAAZAQEBAQEBAQAAAAAAAAAAAAACAQMABAX/xAAiEQACAgICAgMBAQAAAAAAAAAAAQIRAxIhMQRBE1FhMiL/2gAMAwEAAhEDEQA/ANAJE8SqAJE4Sr4x9WicZkCS04lCMiYXrjqJLbVxRBa1ANNybQbCVbJSLmC01SM+KrIXEVx8lx78Tn3CvvV2JqWwlRBJiqQWsDYf2ifKikR281wNMdmCVkcS5AO7nh713v8AP+irm2pPFpStBpk68OJ8F0TbgOVfeoPTrvSriEwzk5kpvSKN0i7fXHEjpEjLh3qPeTHuVOJfS1UpsjSwDaL3iMKKqEmSJ+UbKd/cnF0GSJCVEFkyI2RWiNhAF26utcE4JUFsZcSuIlxTGQDcqohciqfHiu9Gps0XWP42LnRLtSbkIxpj41P6JNMKup2xWGJNMStDCmmzqanblQ6JMbHird1mTG2XFXU7czuljWmwgUPPCh3KmlK1Wm7I5xDWsJo3YCSa47Fm7VYng0Iu+0Qz7RCLjyaRmqORnBdLkaOyOoMCfZa6TxjBXTYn7Gv/AHaeDiD4K6s7cJ0bkuspVEgF46PVZEvnaFzp+1THNQjGuo6wH5Qu/lCc6EbkM2cK6nWIzbgF1svchug4ppgO9dqdZI6VcvqMWOTSXbldTieyfFFE6qulxThOqkQtBaE8WlVXTLvTKkZbi0p4tKpunS/KFUFovW2gJs1sjb6TiODWPeduxoKpRaihWic51pTfhWuyvNNAaL5tvjwusmk4BrWHvEhbREZIXOd1Lo9XrAuOGNQMM67Vm4dJUI6zN9L7anPECqmw21+xrjxDJHDH2WkJV+AdmgjZgnhihWWeQ0pC8/us8HuarWz2eV3qsHtyAeLA9JRA5AQSiNmKsIdESOGMkLD+3KP9tS49XSR1px2xxhv8xz1ooA3Klk6mx2qv9FYM1bjpRz5X8bzYzzhaxEZq9AM2F/62SWX+Y4q6h2KxwOZBA3kEDmmNnjOAkYTuD215VqryHRkDDVtniad7Y2A8wFNEg7FUkGzONaT6sn/zkpzDaIn5JJ/gyHj82PBzwfBaEPG9OT1R1mfboyX6jKcZCDyDCPFHboh/1oxwLHu8Q9vuVykrqjrKsaGG2Rw9lsdOT2uPiit0RHtvk7w9zPBhAU9JdRCtm0PER9Gx3F7b55uqVElsF3BoDR+iAB4K9Q5HDbRFo6zI2qyneVWSWfFa21MbsVLa433uo2Mimbnuaa9gYcMkaKpGYup1E6iVF80+mBmHVPYVV2u3RxFofKGXhUXgaGlK4jtCuJG4FYnXs0EB2EPH8uiUY26I3SL1mkozlPEf2h8SpDZScrp7DX4LzqyTwZyVwrlSuJr8VZRWiwbQ/t/stvi/TP5K9Gxlbe9KOvL4pkV0YhpFfaI3qq0PaLMZWiFz7xvVDi6lLp2ZblPtMsbS2/MIyWtoC4gEY40r+KI6O6L8i1uiUZW71y+N6iMtDD6NpYe9pSlrce4SRuutLsQDWnYr8cg/NEkkguphShPiuOgG5Oa3r8KH7qOWqI0ZAdZgmGznep5amlqQbK6SF31nD2btf4gUwWces+U/tXfsAKyLEJzFUSyIyzw7Q4+3JI/we4o9nhs7AS2ONrqtwbG3rYOqSQMxh+8Vx8ajTNFcfckFstzaS0NOIDgSDkCA4tJHKncuG3cdg9ypppata29UNqGitAATePiSUS2R3XANkDgWRmoIIBdG0ub2gkg8QuoOxbjSB3ordKuG1ZwOcDTvxRI3u2pJEbNNHp142qXDrO8bVlWg7/BEEfFLkDSNmzXBwBxxoadtFnTrdbHPqbQbv1Q1rdm8NBz4qudFgcTkVXtCMpNHRgmb21aetLWNMVoa12BIlBe1wu5ZEjHaEKLXyduEsUT+MbiPA/BUEUvSMFdhp/C34pfkwOxJWZ0jWw69QuzY9vZQhSWa1xnK9yWXs1lA2Ke260VcQBvKasLo0DNZd17kVJj0845V5FZW1WloAoQakNwNc1Z2T0QqmQuxpaTcfBd/6jIf7hVrXo8ILslSELTGtjbO65JWtAcKZFU0vygR7nc03WXU2W1TGQWhjG3WgC65xwGO0Kp//MXbbYO6An/cCjv6LSJU3ygt2M/i/ooUmvpr6A5oo+TJvrWt57IgPe8pH5M4f8eb91vwXcnUi1ouJ1El80+mNIWI+UBnzVnNaUe4V3VaP+K3JWM+UFn5vEf86nNknwTx/wBIj6MG5x3ndnuTCTvPMogCZKMF6zMuNTH0tkWWIeMh/huPkrHX6ofD+rI5OVPqg789g9p2/bG8K6+UYdaA8Jhycz4oP+0cujJmQ/gke4qe2pie6ucDqgEnFpOJrtpRVlDxVxBUwEdb6GcY3buFT1aY9tVoBo9KaOsOw/dRy1R4DW4d7feApdF5UasEWppajEJpCSCALUxzVIITXNSI2RXNWb1sqLlCRiciR7lqnNWY1wGDO0+4pLsEuin0VO8ysaXuIJNQSaHquI8aLjrZIHvF7AF9Oq3ZWmxc0P8ATs7T9ly49vXl/wBXzWi7M30aHRZLo2OObmNJ2YkAqdGxRNDN+Zj/AFbPshWLAoU6xqIAutCa4u2DmfgqSxS+iew+5U4crMseQQ4gA4dUUOPaSo40Wz6zuY+CElY4SSLGzAUoN/kFMY1V8LS3I17VKjlP4/umjFljEE3SPod4QWTplumq0e0PcUrDR2DMdo960EB6oWdhdiO1XMU2ARgdInByk2R+arBMpdjkz7vNaBPOdfNO2mOWXo7RM0CS6A2R7QBlgGkLz+1awWyp/PLSe20TH7y1evrqyycbQ8cr/wAFhqVkA3vA8QFIjPS4dEAjrT2p3tWmXyIRDoGDaJTxM9o/5qXZzgPxtKNVIysvqLlESi5RfO1Pp2Moslr3F+aezMDzvj7y2F1ZjXlv5nIdz2fzAPNWK/0jm+DzQJsowyTwmytwP9F6gErVfC2QfrAMt4I81pPlGZ1YDudMN+ZafJZbQZpabOf8+LZvkaFr/lDiHRxupj0pFeF1xp7uSMv7Ry6ZhKfihVxoxouU6tSyYYNIPo7TtVT+M1f6FbWjRUtrIBi0txZkNtfBMMje6PNWQu3xs8WBTyFWaENbPZjvii8YlcUWFDsEQm0Ri1cLFaDYEtTS1HuIVob1Xdh9ytBsC5qy+uYwZ2+RUJ1rcQOseZ4fFTdC6Kkt7THEQXMlIN4kDIDjvSoLkUeh2/PM7T9kp9w9JLhslPcQSEcSWaC9IbXE50T7hja2bpCTVpuh0YqBQ1NaYcRWxsJsT3tLLax8kzJg2Po5G3SYnkBz3CgN4XQDSuzCldIp2ZuSolaH+ijH+Wz7IU9qj2CG61o3NYOTQpTURBWBIhHhYnmJKgWQnIZKnuhQXwqUWyMHIgckGIwiUo6xl4oc8mXtBTGwqFpKOlz2wuIHhmBxBriR3gkEcwrFsiorAfSH+Y7+KjvvK2YVxWTmSKwsD8+7zVSxysrAK/vD3hOIGeW65urIeNokd9v4rFwYzs/Ws+2FrdZ21lDt7pMKca1qsnYKdPHXISNPJwPkrErPVYDgOzzKNeUeyODgCDhRSUjM01FyickvLqe+xtFnNcmVsc43Fp5PjK0qq9J2RsrJYnEgPAFQKkYDEclK6Os8daUn5H+i9BZqPZxnJKeyg94UlmqNlHqPd7Tx5UWtoFs810X9PCd0sRy3PaVu/lEiJhbTZOP5TlcWbV2ysNWwNqMiS4nxVm6InNoONcRXGlEW7aOTPJ4IyQPmWnuds71favwkSNrAGkuzuFuBoKlxFKLdCDgB2ABOER3/AI7gnt+Aa/Sv0TEegiaRQiOOoOYIbiDyVpRcZHT8du3vT6IqIrGEJUTqLiVEsQao9oPVIG6mPZxVLrjK5sTS1zmm+BVri05O2grNQaXtLcrRN3yPd4ElJQDsFi0FPUdXAUJzpn+jWuStNDaNtVmZL0UxidI5zr7Qai8BhiRtCzmldMWgkVnkxbU0eW7SPVpuVRaZnOpec52HrOJ2neu1ZLRMtmpobUutsQJzv0aSdpPXJqgDQkMYDjaIyQ7Bw6Qi82hNA1vFu3aohHV/aPuamyPN0DYHOPeQ0H7I5Jcv2FUjVnWmCNoqXvOFSAACab3HgujWGV/0VklfXI9c/ZaRs3rPau6Qkhna6J10u6juqx1Wk1pR4NMWjEYrex6xz7ejd2xR/dAXLGiuTI9httucBWyBvF0ob4YnwUp9otn/AKzT7NpA+1GjN1mlGcUBoCfQeMhXY8KZDrMRUGBmB9V0rfvFLRgsqJdKzxgl9lc0DEnpoHYcwUODTz5aNZBPedUNutiJrwDnDxV+7T8ZNXWUH/VPmwp8embMOv8AkxaQcwYyRgf0BsBU0Z1oyw0Zauk6Vxt4yq3o7PdoNlxryB3CqsI9Os+pMKYGtnkwO40yK1X/AHFAc2TdzYz98J7dLWY7XjtjH3XFdo/o7YyzdYoQes4tFMzHMDXbhcIQbTpaCctETw4tNXelWmyt5o4rV23TFljjdIZT1Rl0cgqagAVoQMSFkNIa6uOEUYHF2f8AXwRcaOQawYuf7Y/lsV0xizuibc+XrPNXUrhlS88U/hWlgJKHsTHtarjRLMP2h5KBGxWdjF1h7zyA+C0SM2zxrTzquj4iQ/Y+KzOhWXrTGDkXGvZQrR6azZwY/wAbvwVBqwK2yIcX+EbiuiN9Hp9nbgnujBzCJDEidCnRjZokl1cWFHtsSEYxWtEVNVoljbg3JXU5cVolnE0hPTSrRLOLicuLqJYlxdXFaJZwhNKchNcaYihVollBrk35ge233OWPYFsta4yYMqkPBw3Y/FY2MrRLglkLSnpN9n7zlBk2dnmVO0qes32fvOVVPamimOzZjniPeg+xBHej3n7qE92HPyQxOXYBhpXM8abO5No4qWjqbJeivpme15LYNWN0ZF89GdzvIrZMThyGXAQ5HsPuKMD1ne0UB+R7D7kRhxd7R8k6BYeq7I7qn8eq9MXJT1T2/dcrRCaHIrSoociNerQbAawO+Yd3fbYsoXLQ6xy/MHtH2mrJGZY5OzSHRrtXjl7A/mS/ELYWV2S8us2sDIg30i4AgtDTtdUY5bfFXuitcgWlz47oaQD1s+IBFdiwp2aPo9HiCkSvpFIdzH+DSVmbBrLG4B1H0ORoPipthtvSaPlm+s21kdl+YN8AFomjNpo8w04/EDdHXmT8FW6iRX7czg2Q/wAJHmjafm64un1KEbsSfNLUY9Dag+leo4UOGZbtXRdCknR63FZ0nxgIEGm2U6zXDsIPvomT6UYT1TXuIxWlmGrLtIpLiFHqs6mlIuTWvrkuoljklxJWiWIphcK02n+vwKcSo89pjZi9zW7i4gcqrqODpKon1hhbkS72Qfe6gVfPrI8+iwDi4k+Ap71LSKoyZpnFBltDWirnADeSAOZWPl0nM71yODQG+Ix8VEc2pqTU7zieaLmhrEzVz6ciGTq+yCfHLxVbPrEfVZ3k08B8VSliaEd2NYkO0lpWVzTV1ODRTxz8VlrTY2vNTUnDE9gHkr22CrcFCugIObNY4o/RVvsNXB1TgG0rjS6AMK9iTbEBu2DjgKeSsXtTbn4CDmxrFFdIhGyrj7Mp4GHkkG8FNhaIgRWZwIIIBGRUx9vnaG3S12GN4DedgpsojMZvR22euTU45WjOeGLIrdMy0N6IEEUqCR8VJGnmNoXMf1utgKgYltDWn1fFP/6eONUySw1oN29aLyX9GL8ZemSY9PwH1y32muHjSikm3Rub1ZGnEesNzlUSWCmyqjusXVLbpxINBXGl74rReQvZnLxmumaxrwcjVEBWLmsBvFwvNJpkdwA8koTMMGzP7yT71ovIgZvxpmg1md8x+0PeFjw4VArSqtbZNK9jWufe6xLiQ3ZdoBQdqiWezBmQqd5WWXJFu0aYsUkuSwsNlFctmZzVpZrCzaKqLYirSAry3ybtEpkQpThTBFs9seywmEXQzo3tyNQHE1oa8UONyVtwhcBup4rVMycTEWpgBNEbQNGyV4fBK1MRNGtoaoqRrKPBp2y4KPLM4HDJBY5OOK02MND0uqSprRrFEPRvO9ltBzdRV0+sch9FjW8XEuPktnKKCoSZpnOG9AntUcY6zmt7SBXuzKx0+kpn+lI7sb1R/DSqiV4IPIjRYX7NXaNYoh6N53stoObqeCrp9YpD6LWt9ol3gKBUtUrqDmxrFElz6QlfnK7sb1B/DSqh0HPmnXUrqDkaKH0cqurtFwqbF1O1XO9IlKiOwtTh/FVw1TgnXOKmxVEhWlpIUQQK1ewIfRIOQ4xILoQE7o6jLn8ApbowEN53I2x0RnwkCpy34ALoiFKk4cMFLslpew1a4tOXVJGC44E4k1O/b3lX0HmxkLRsHeURxOxcDwBv/G9Dzz5KIrCM9rxTq7uaG1vcng7glYaHUHauZ5DvXHOAzxO5OFSMcFbJQMtCGYwcKI12pTsBgM0uA0yHLZ2geSj9EMzgFNtLqCqjsjLsTlsCEmJIPZ+AUtpohwNpijNbXsVTM5IJG9EtvoJoomWx9WpXwDXkzdoGKJYW41TbQ3GiJANgUTG0Tw5SI8lCClx5J2Z6h0iu0XQ3gq2aUNouABEDErvBByEojF0cSnlu8rl7cFNhaiHYmudRccd55LgB2DzKlloRemFyvNGao2qehbC4A+s/qN8cT3LW6N+TUChnlr+jGPvu+CjmjOUox7Z5ywFXOjtWLVPQshIafWcLrebs+6q9Z0Zq5ZoMY4Wg/WPWd+87EdytQjbZk/IXpHk0/wAn1raKjo3cA417qgKitei5YjSSJ7eLgac8l7rVcfGHCjgCDsOSnL6Oj5LXaPn94G/uCG5rvxmvYdOavaOa0vlDIf0g64a9nrcl5npWzxdJcsrpJRsqzE+yBieQXSTj2enHkU+ildDTtTLis7VoqdmL4nju99Mu9QXM34KWaArgGfwTXGuAGCe8jcO9cDDuXWdQ1rU7oynNoM0XP4BdsdQAN3rpNcBgjFM7FdiUcuBvEpBm0ojW7UwtJKuxGhpNchgkRTtRHupsQaVSQGDkZtKC1+OA5o8kVURsIaFzqzkJhrmpbSNijRNqpQauCMJKFbMApCBaArYa5KXo6lGjZROnw8k2A1RsYUIzXoICdVNGbRZjgE6m8pJLrNULsHNdLT/ZJJBuhJCbHuFTwVvo/VC1z4iJzWn1pOoORxPcEkkbZlmyOHRq9GfJowUM8xd+jGLo/eOJ5Ba3RugbNZ/ooWg/WIvO/eOKSSVHjlklLtliUr24d5SSSXDAd7Vx1OwcUklNruzjP6V1xslnqL/SO+rH1uZyHNZi0a2261m7ZouiZ9YCp73u6o7gkks5TajwfQWCEOewVm1RLz0lqmc92ZAcTze7yorGG32aznorNEZZD6sLbx/af/dJJYw/06Y5P/Lb9EyPQtstP07xZoz/AOOKjpSNzn5N7q9in/8AY9iuXOhrveXOLyd96uHYMEkl7FiinR4JZ5v3RQW/5L2YmGZwOxsgDh2Aih51WV0lqfbYa1ivt3x9Ycs/BJJHNjUY7I0x+TO6fJSOhumjmkHcRQjuKafwAuJLzp2fROXTt5J3cuJKnCr+Pgk51AkkkEjOkJKIxlUklr0jL2HDAENzKpJIWWgrBRdLkkk4ozkdjbVAtfBJJc+yrorZ2k5p8bKJJLmUJdwQykkqgM//2Q==');\">
        </div>
        <div class=\"col-md-4\">
          <h1>{{ property.title }}</h1>
          <h2>{{ property.rooms }} pièces - {{ property.surface }} m²</h2>
          <div class=\"text-primary\" style=\"font-size: 4rem;font-weight: bold;\">{{ property.formattedPrice }} €</div>
          <a href=\"#\" class=\"btn btn-primary\">Contacter l'agence</a>
           <div id=\"contactForm\" class=\"mt-4\">
            {{ form_start(form) }}
            <div class=\"row\">
              <div class=\"col\">{{ form_row(form.firstname) }}</div>
              <div class=\"col\">{{ form_row(form.lastname) }}</div>
            </div>
            <div class=\"row\">
              <div class=\"col\">{{ form_row(form.phone) }}</div>
              <div class=\"col\">{{ form_row(form.email) }}</div>
            </div>
            {{ form_rest(form) }}
            <div class=\"form-group\">
              <button class=\"btn btn-primary\">Envoyer</button>
            </div>
            {{ form_end(form) }}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class=\"container mt-4\">

    <p>
      {{ property.description | nl2br }}
    </p>

    <div class=\"row\">
      <div class=\"col-md-8\">
        <h2>Caractéristiques</h2>
        <table class=\"table table-striped\">
          <tr>
            <td>Surface habitable</td>
            <td>{{ property.surface }} m²</td>
          </tr>
          <tr>
            <td>Pièces</td>
            <td>{{ property.rooms }}</td>
          </tr>
          <tr>
            <td>Chambres</td>
            <td>{{ property.bedrooms }}</td>
          </tr>
          <tr>
            <td>Etage</td>
            <td>{{ property.floor }}</td>
          </tr>
          <tr>
            <td>Chauffage</td>
            <td>{{ property.heatType }}</td>
          </tr>
        </table>
      </div>
      <div class=\"col-md-4\">
        <h2>Spécificités</h2>
      </div>
    </div>
  </div>
{% endblock %}
", "property/show.html.twig", "/home/ali/MaSuperAgence/templates/property/show.html.twig");
    }
}
